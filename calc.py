from math import sqrt


def sumar(a, b):
    return a + b


def restar(a, b):
    return a - b


def mult(a, b):
    return a * b


def division(a, b):
    return a / b


def raiz_cuadrada(num):
    return sqrt(num)


def potencia(a, b):
    return a ** b


sumar1 = f"Resultado = 1 + 2 = {sumar(1, 2)}"
print(sumar1)
sumar2 = f"Resultado = 2 + 3 = {sumar(2, 3)}"
print(sumar2)

restar1 = f"Resultado = 1 - 2 = {restar(6, 5)}"
print(restar1)
restar2 = f"Resultado = 8 - 7 = {restar(8, 7)}"
print(restar2)


def menu():
    print("""1. Sumar
2. Restar
3. multiplicar
4. dividir
5. raiz cuadrada
6. elevado a la potencia
7. Salir""")


def in_number():
    try:
        num = int(input("Ingrese un numero: "))
        num2 = int(input("Ingrese otro numero: "))
        return num, num2
    except:
        print("Error, no es un numero")
        in_number()

def opcional():
    menu()
    opcion = int(input("Ingrese una opcion: "))
    if opcion in range(1, 6):
        num1, num2 = in_number()
    else:
        num1 = int(input("Ingrese un numero: "))
        num2 = int(input("Ingrese el exponente: "))
    while opcion in range(1, 8):
        if opcion == 1:
            print(f"Resultado = {num1} + {num2} = {sumar(num1, num2)}")
            opcional()
        elif opcion == 2:
            print(f"Resultado = {num1} - {num2} = {restar(num1, num2)}")
            opcional()
        elif opcion == 3:
            print(f"Resultado = {num1} * {num2} = {mult(num1, num2)}")
            opcional()
        elif opcion == 4:
            print(f"Resultado = {num1} / {num2} = {division(num1, num2)}")
            opcional()
        elif opcion == 5:
            print("Raiz cuadrada")
        elif opcion == 6:
            print(f"Resultado = {num1}  {num2} = {potencia(num1, num2)}")
            opcional()
        elif opcion == 7:
            print("Salir")
            break
        else:
            print("Opcion no valida")
            menu()


opcional()
